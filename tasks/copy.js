// libs
var gulp = require('gulp');

//
// ---- tasks
//

// gulp.task - 'copy:img'
gulp.task('copy:img', function () {
  return gulp.src('./src/images/**')
  .pipe(gulp.dest('./build/images'));
});

// gulp.task - 'copy:php'
gulp.task('copy:php', function () {
  return gulp.src('./src/php/**')
  .pipe(gulp.dest('./build/'));
});

// gulp.task - 'copy:fontawesomefonts'
gulp.task('copy:js', function () {
  return gulp.src('./src/js/**')
  .pipe(gulp.dest('./build/js'));
});

// gulp.task - 'copy'
gulp.task('copy', function () {
  gulp.start(
    [
      'copy:img',
      'copy:php',
      'copy:js'
    ]
  ); // starts only after 'copy:content'
});