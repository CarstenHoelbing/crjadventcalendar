<?php
/*
 * #Plugin Name: CRJAdventCalendarPluginWP
 * #Plugin URI: http://www.campusradio-jena.de/
 * #Description: Advent Calendar als WP Plugin fuer das Campusradio-Jena
 * #Version: 2.0
 * # Update: 2017-11-04
 * #Author: Carsten Hoelbing
 * #Author URI: http://www.hoelbing.net/
 * #License: GPL 2.0, @see http://www.gnu.org/licenses/gpl-2.0.html
 */
/*
 * Copyright 2016 Carsten Hoelbing (email: carsten@hoelbing.net)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
// ##################################################################################################

wp_enqueue_style ( 'CRJAdventCalendarPluginWP', plugins_url() . "/CRJAdventCalendar/css/CRJAdventCalendarPluginWP.css" );

/* init Plugin
	es werden mehrere Variablen der Datenbank hinzugefuegt wenn sie nicht schon anglegt worden sind
*/
function PluginInit() {

	// wenn Variablen noch nicht vorhanden sind dann anlegen
	if (get_option ( 'crj_advent_link_tag01' ) != "") {

		// fuer jeden tag ein variable mit dem entsprechenden link darin
		add_option ( 'crj_advent_link_tag01', '' );
		add_option ( 'crj_advent_link_tag02', '' );
		add_option ( 'crj_advent_link_tag03', '' );
		add_option ( 'crj_advent_link_tag04', '' );
		add_option ( 'crj_advent_link_tag05', '' );
		add_option ( 'crj_advent_link_tag06', '' );
		add_option ( 'crj_advent_link_tag07', '' );
		add_option ( 'crj_advent_link_tag08', '' );
		add_option ( 'crj_advent_link_tag09', '' );
		add_option ( 'crj_advent_link_tag10', '' );
		add_option ( 'crj_advent_link_tag11', '' );
		add_option ( 'crj_advent_link_tag12', '' );
		add_option ( 'crj_advent_link_tag13', '' );
		add_option ( 'crj_advent_link_tag14', '' );
		add_option ( 'crj_advent_link_tag15', '' );
		add_option ( 'crj_advent_link_tag16', '' );
		add_option ( 'crj_advent_link_tag17', '' );
		add_option ( 'crj_advent_link_tag18', '' );
		add_option ( 'crj_advent_link_tag19', '' );
		add_option ( 'crj_advent_link_tag20', '' );
		add_option ( 'crj_advent_link_tag21', '' );
		add_option ( 'crj_advent_link_tag22', '' );
		add_option ( 'crj_advent_link_tag23', '' );
		add_option ( 'crj_advent_link_tag24', '' );

		// variable wie der status ist anlegen
		add_option ( 'crj_advent_cheatMode', '' );

		add_option ( 'crj_advent_text_title', 'Campusradio Adventskalender' );
		add_option ( 'crj_advent_text_subtitle', 'Das Campusradio Jena wünscht allen eine besinnliche Adventszeit' );

	}
}

add_action ( 'init', 'PluginInit' );


/**
 * Register a custom menu page.
 */
function wpdocs_register_adventskalender_menu_page() {
	add_menu_page (
			'Adventskalender',
			'Adventskalender',
			'manage_network_users',
			'AdventskalenderEinstellungen',
			'ShowBackendSettingsPage'
	);
}


	// Hauptpunkt anlegen (im Admin Bereich)
add_action( 'admin_menu', 'wpdocs_register_adventskalender_menu_page' );


/**
 * erstelle Shortcode
 * dh. wenn auf einer Seite der tag "[AdventCalendarWP]" eingefuegt wird,
 * wird an dieser Stelle der Code ausgegeben
 */
function advent_shortcode() {
	return show_FrontendPage();
}
add_shortcode( 'AdventCalendarWP', 'advent_shortcode' );

/**
 * Backend -> save Links To Advent-Posts
 */
function updateLinksToAdventPosts() {

	// status: cheat mode
	update_option ( 'crj_advent_cheatMode', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_cheatMode'] ) ) ) );

	// text
	update_option ( 'crj_advent_text_title', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_text_title'] ) ) ) );
	update_option ( 'crj_advent_text_subtitle', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_text_subtitle'] ) ) ) );


	update_option ( 'crj_advent_link_tag01', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag01'] ) ) ) );
	update_option ( 'crj_advent_link_tag02', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag02'] ) ) ) );
	update_option ( 'crj_advent_link_tag03', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag03'] ) ) ) );
	update_option ( 'crj_advent_link_tag04', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag04'] ) ) ) );
	update_option ( 'crj_advent_link_tag05', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag05'] ) ) ) );
	update_option ( 'crj_advent_link_tag06', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag06'] ) ) ) );
	update_option ( 'crj_advent_link_tag07', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag07'] ) ) ) );
	update_option ( 'crj_advent_link_tag08', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag08'] ) ) ) );
	update_option ( 'crj_advent_link_tag09', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag09'] ) ) ) );
	update_option ( 'crj_advent_link_tag10', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag10'] ) ) ) );
	update_option ( 'crj_advent_link_tag11', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag11'] ) ) ) );
	update_option ( 'crj_advent_link_tag12', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag12'] ) ) ) );
	update_option ( 'crj_advent_link_tag13', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag13'] ) ) ) );
	update_option ( 'crj_advent_link_tag14', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag14'] ) ) ) );
	update_option ( 'crj_advent_link_tag15', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag15'] ) ) ) );
	update_option ( 'crj_advent_link_tag16', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag16'] ) ) ) );
	update_option ( 'crj_advent_link_tag17', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag17'] ) ) ) );
	update_option ( 'crj_advent_link_tag18', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag18'] ) ) ) );
	update_option ( 'crj_advent_link_tag19', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag19'] ) ) ) );
	update_option ( 'crj_advent_link_tag20', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag20'] ) ) ) );
	update_option ( 'crj_advent_link_tag21', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag21'] ) ) ) );
	update_option ( 'crj_advent_link_tag22', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag22'] ) ) ) );
	update_option ( 'crj_advent_link_tag23', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag23'] ) ) ) );
	update_option ( 'crj_advent_link_tag24', htmlspecialchars ( strip_tags ( stripslashes ( $_POST ['crj_advent_link_tag24'] ) ) ) );

}
/**
 * ********************************************************************************************************
 *
 * Widget fuer die Sidebar
 *
 * ********************************************************************************************************
 */

class AdventCalendar_Widget extends WP_Widget {

  // Frontend-Design Funktionen
  public function __construct(){

		$widget_ops = array(
		    'classname'   => 'user-profile',
		    'description' => 'Zeigt ein bild in der Sidebar an.',
		);
		$control_ops = array(
		    'id_base' => 'user-profile',
		    'width'   => 195,
		    'height'  => 245,
		);
		$this->WP_Widget( 'user-profile', 'Adventskalender', $widget_ops, $control_ops );

  }

  // Funktionen für die Ausgabe des Codes auf der Website
  public function widget( $args, $instance ) {

		//global $app_id;
	  extract( $args );

		$title 		= apply_filters('widget_title', $instance['title']);
		$width		= $instance['width'];
		$height		= $instance['height'];
		$url_to_page	= $instance['url_to_page'];
		$url_image	= $instance['url_image'];

	//	echo $before_widget;

		echo '<div class="widget">';
		echo '  <a href="'.$url_to_page.'">';
		echo '    <img src="'.$url_image.'" alt="'.$title.'" title="'.$title.'" width="'.$width.'" height="'.$height.'"/>';
		echo '  </a>';
		echo '</div>';

	//	echo $after_widget;

  }

	// Erstellt das Kontroll-Formular im WP-Dashboard
  public function form( $instance ) {

		/**
		 * Set Default Value for widget form
		 */
		$default_value	=	array("title"=> "Adventskalender", "width" => "195", "height" => "245", "url_to_Page" => "", "url_image" => "" );
		$instance		=	wp_parse_args((array)$instance,$default_value);

		$title		= esc_attr($instance['title']);
		$width		= esc_attr($instance['width']);
		$height		= esc_attr($instance['height']);

		$url_to_page	= esc_attr($instance['url_to_page']);
		$url_image	= esc_attr($instance['url_image']);

		?>
		<p>
		    <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Titel:', 'crjAdventkalendar'); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	        </p>

	        <p>
		    <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e( 'Breite des Bildes:', 'crjAdventkalendar' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>" type="text" value="<?php echo $width; ?>" />
		</p>

		 <p>
		    <label for="<?php echo $this->get_field_id('height'); ?>"><?php _e( 'Höhe des Bildes:', 'crjAdventkalendar' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id('height'); ?>" name="<?php echo $this->get_field_name('height'); ?>" type="text" value="<?php echo $height; ?>" />
		</p>
		 <p>
		    <label for="<?php echo $this->get_field_id('height'); ?>"><?php _e( 'Url zu der Seite', 'crjAdventkalendar' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id('url_to_page'); ?>" name="<?php echo $this->get_field_name('url_to_page'); ?>" type="text" value="<?php echo $url_to_page; ?>" />
		</p>
		 <p>
		    <label for="<?php echo $this->get_field_id('height'); ?>"><?php _e( 'Url vom Bild', 'crjAdventkalendar' ); ?></label>
		    <input class="widefat" id="<?php echo $this->get_field_id('url_image'); ?>" name="<?php echo $this->get_field_name('url_image'); ?>" type="text" value="<?php echo $url_image; ?>" />
		</p>

		<?php
  }

	  // Updating der Widget-Funktionen
	  public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] 		= strip_tags($new_instance['title'] );
			$instance['width'] 		= strip_tags($new_instance['width'] );
			$instance['height'] 		= strip_tags($new_instance['height'] );
			$instance['url_to_page']	= strip_tags($new_instance['url_to_page'] );
			$instance['url_image']		= strip_tags($new_instance['url_image'] );

			return $instance;
  	}

}
// Die Registrierung unseres Widgets
function adventcalendar_widget_init() {
    register_widget('adventcalendar_widget');
}
add_action('widgets_init', 'adventcalendar_widget_init');

/**
 * ********************************************************************************************************
 *
 * neuer Wordpress-Daten-Type - Adventskalender
 *
 * ********************************************************************************************************
 */
// most infos from: http://blog.teamtreehouse.com/create-your-first-wordpress-custom-post-type
// http://pressengers.de/tipps/wordpress-anpassen-custom-post-types-erstellen/
// http://www.elmastudio.de/wordpress-custom-post-types-teil2-selbst-individuelle-inhaltstypen-und-taxonomies-anlegen/

add_action('init', 'adventtype_register');

 function adventtype_register() {

 	$labels = array(
 		'name' => _x('Adventskalender', 'post type general name'),
 		'singular_name' => _x('Eintrag', 'post type singular name'),
 		'add_new' => _x('Neuer hinzufügen', 'portfolio item'),
 		'add_new_item' => __('Neuer Adventskalender-Eintrag hinzufügen'),
 		'edit_item' => __('Eintrag bearbeiten'),
 		'new_item' => __('Neue Eintrag hinzufügen'),
 		'view_item' => __('Eintrag anzeigen'),
 		'search_items' => __('Eintrag suchen'),
 		'not_found' =>  __('Keine Eintrag gefunden'),
 		'not_found_in_trash' => __('Keine Eintrage im Papierkorb gefunden'),
 		'parent_item_colon' => 'Master Eintrag'
 	);

  // rewrite rules
	$rewrite = array(
		'slug' => 'adventeintrag'
	);

 	$args = array(
 		'labels' => $labels,
		//should they be shown in the admin UI
 		'public' => true,
		//should we display an admin panel for this custom post type
 		'publicly_queryable' => true,
		//should we display an admin panel for this custom post type
 		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
 		'menu_icon' => 'dashicons-book-alt',
		'menu_position' => 4,
		'can_export' => false,
		'has_archive' => true,
		'exclude_from_search' => false,
		'query_var' => true,
 		'rewrite' => $rewrite,
		//WordPress will treat this as a ‘post’ for read, edit, and delete capabilities
 		'capability_type' => 'post',
 		'hierarchical' => false,
		// tags/schlagwoerter werden in der rechten Sidebar mit angezeigt
		// 'taxonomies' => array('post_tag'),
		//which items do we want to display on the add/edit post page
 		'supports' => array('title', 'editor'), //,
    'map_meta_cap' => true,
    // add supports for rest api
    'show_in_rest'       => true,
    'rest_base'          => 'adventeintrag',
    'rest_controller_class' => 'WP_REST_Posts_Controller'
 	);

 	register_post_type( 'adventeintrag' , $args );
 }

/**
 * ********************************************************************************************************
 *
 * Frontend
 *
 * wird auf der entsprechenden Seite angezeigt
 * nur eine Seite "adventskalender"
 *
 * ********************************************************************************************************
 */

function show_FrontendPageTest() {
	$post_id = 16572;
	$queried_post = get_post($post_id);
	$content = $queried_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);

	echo $content;
}

function getAdventInfo() {

	$TageLang = array('1' => 'Montag','2' =>'Dienstag','3' =>'Mittwoch', '4' =>'Donnerstag', '5' =>'Freitag', '6' =>'Samstag', '0' =>'Sonntag');
	$MonateLang = array('01' =>'Januar', '02' =>'Februar', '03' =>'März', '04' =>'April', '05' =>'Mai', '06' =>'Juni', '07' =>'Juli', '08' =>'August', '09' =>'September', '10' =>'Oktober', '11' =>'November', '12' =>'Dezember');

	$time_now = mktime(date("H"), date("i"), date("s"), date("n"), date("j"));
	$time_adv = mktime("0", "0", "0", "12", "25");
	$time_div = $time_adv - $time_now;
	$time_div = $time_div / 60 / 60 / 24;

	$html_output = "heute ist " . $TageLang[date("w")] . " der " . date("d") . ". " . $MonateLang[date("m")] . " " . date("Y");

	if ($month == '12') {
		//$day = date("j");
		if ($day <= 22) {
			$html_output .= " - noch " . sprintf("%d", $time_div + 1) . " Tage bis Weihnachten ";
			$html_output .= "- Sucht das Fensterchen f&uuml;r heute!";
		} else if ($day == 23) {
			$html_output .= "Morgen ist Heiligabend!";
		} else if ($day == 24) {
			$html_output .= "Heut ist Heiligabend!";
		} else if (($day >= 25) and ($day <= 30)) {
			$html_output .= "Frohe Weihnachten - Alle Fensterchen d&uuml;rfen ge&ouml;ffnet werden";
		} else {
			$html_output .= "Weihnachten ist vorbei!";
		}
	} else {
		$html_output .= " - noch " . sprintf("%d", $time_div) . " Tage bis Weihnachten";
	}

	return $html_output;
}
// sollte im footer aufgerufen werden
// einbinden des piwik code
function getHtmlPiwikCode() {
	?>
		<!-- Piwik -->
		<script type="text/javascript">
			var _paq = _paq || [];
			_paq.push(["trackPageView"]);
			_paq.push(["enableLinkTracking"]);

			(function() {
				var u = (("https:" == document.location.protocol) ? "https" : "http") + "://stats.stura.uni-jena.de/";
				_paq.push(["setTrackerUrl", u + "piwik.php"]);
				_paq.push(["setSiteId", "12"]);
				var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
				g.type = "text/javascript";
				g.defer = true;
				g.async = true;
				g.src = u + "piwik.js";
				s.parentNode.insertBefore(g, s);
			})();
		</script>
		<!-- End Piwik Code -->
		<?php
}

function getHtmlAdventskalenderAsTree() {
	?>
		<div id="content_tree">
			<map name="Beispiel">
				<area alt="01" onMouseOver="popLayer(1)" onMouseOut="hideLayer()" shape="circle" coords="148, 425, 15" href="<?php echo $page_name; ?>?day_nr=01">
				<area alt="02" onMouseOver="popLayer(2)" onMouseOut="hideLayer()" shape="circle" coords="398, 314, 15" href="<?php echo $page_name; ?>?day_nr=02">
				<area alt="03" onMouseOver="popLayer(3)" onMouseOut="hideLayer()" shape="circle" coords="472, 579, 15" href="<?php echo $page_name; ?>?day_nr=03">
				<area alt="04" onMouseOver="popLayer(4)" onMouseOut="hideLayer()" shape="circle" coords="272, 78, 15" href="<?php echo $page_name; ?>?day_nr=04">
				<area alt="05" onMouseOver="popLayer(5)" onMouseOut="hideLayer()" shape="circle" coords="174, 526, 15" href="<?php echo $page_name; ?>?day_nr=05">
				<area alt="06" onMouseOver="popLayer(6)" onMouseOut="hideLayer()" shape="circle" coords="276, 215, 15" href="<?php echo $page_name; ?>?day_nr=06">
				<area alt="07" onMouseOver="popLayer(7)" onMouseOut="hideLayer()" shape="circle" coords="290, 482, 15" href="<?php echo $page_name; ?>?day_nr=07">
				<area alt="08" onMouseOver="popLayer(8)" onMouseOut="hideLayer()" shape="circle" coords="56, 580, 15" href="<?php echo $page_name; ?>?day_nr=08">
				<area alt="09" onMouseOver="popLayer(9)" onMouseOut="hideLayer()" shape="circle" coords="323, 326, 15" href="<?php echo $page_name; ?>?day_nr=09">
				<area alt="10" onMouseOver="popLayer(10)" onMouseOut="hideLayer()" shape="circle" coords="467, 463, 15" href="<?php echo $page_name; ?>?day_nr=10">
				<area alt="11" onMouseOver="popLayer(11)" onMouseOut="hideLayer()" shape="circle" coords="207, 165, 15" href="<?php echo $page_name; ?>?day_nr=11">
				<area alt="12" onMouseOver="popLayer(12)" onMouseOut="hideLayer()" shape="circle" coords="137, 324, 15" href="<?php echo $page_name; ?>?day_nr=12">
				<area alt="13" onMouseOver="popLayer(13)" onMouseOut="hideLayer()" shape="circle" coords="245, 569, 15" href="<?php echo $page_name; ?>?day_nr=13">
				<area alt="14" onMouseOver="popLayer(14)" onMouseOut="hideLayer()" shape="circle" coords="392, 384, 15" href="<?php echo $page_name; ?>?day_nr=14">
				<area alt="15" onMouseOver="popLayer(15)" onMouseOut="hideLayer()" shape="circle" coords="306, 142, 15" href="<?php echo $page_name; ?>?day_nr=15">
				<area alt="16" onMouseOver="popLayer(16)" onMouseOut="hideLayer()" shape="circle" coords="416, 520, 15" href="<?php echo $page_name; ?>?day_nr=16">
				<area alt="17" onMouseOver="popLayer(17)" onMouseOut="hideLayer()" shape="circle" coords="180, 231, 15" href="<?php echo $page_name; ?>?day_nr=17">
				<area alt="18" onMouseOver="popLayer(18)" onMouseOut="hideLayer()" shape="circle" coords="76, 464, 15" href="<?php echo $page_name; ?>?day_nr=18">
				<area alt="19" onMouseOver="popLayer(19)" onMouseOut="hideLayer()" shape="circle" coords="211, 444, 15" href="<?php echo $page_name; ?>?day_nr=19">
				<area alt="20" onMouseOver="popLayer(20)" onMouseOut="hideLayer()" shape="circle" coords="328, 541, 15" href="<?php echo $page_name; ?>?day_nr=20">
				<area alt="21" onMouseOver="popLayer(21)" onMouseOut="hideLayer()" shape="circle" coords="249, 370, 15" href="<?php echo $page_name; ?>?day_nr=21">
				<area alt="22" onMouseOver="popLayer(22)" onMouseOut="hideLayer()" shape="circle" coords="369, 214, 15" href="<?php echo $page_name; ?>?day_nr=22">
				<area alt="23" onMouseOver="popLayer(23)" onMouseOut="hideLayer()" shape="circle" coords="358, 444, 15" href="<?php echo $page_name; ?>?day_nr=23">
				<area alt="24" onMouseOver="popLayer(24)" onMouseOut="hideLayer()" shape="circle" coords="228, 297, 15" href="<?php echo $page_name; ?>?day_nr=24">
			</map>
			<img usemap="#Beispiel" src="<?php echo plugins_url (); ?>/CRJAdventCalendar/images/tree24-01.gif" width="550" height="660" border="0" alt="Baum">
		</div><!-- END  content_tree-->
	<?php
}

function getDataAsArray() {

	// tage
	$day_row_array = array();// array ( array("day" => 0, "post_id" => false));

	for ($day_nr = 1; $day_nr <= 24; $day_nr++ ) {

		// eine 0 hinzufuegen bei kleiner gleich 9
		if ($day_nr <= 9) {
			$day_nr = "0" . $day_nr;
		}
		$day_row_array = array_merge( $day_row_array, array( array(
			"day" => $day_nr,
			"post_id" => get_option('crj_advent_link_tag' . $day_nr),
			"img" => plugins_url () . '/CRJAdventCalendar/images/eule/eule' . $day_nr . ".png"
		)));
	}

	// Mischt die Elemente
	shuffle ( $day_row_array );

	return $day_row_array;
}

function getHtmlAdventskalenderAsDiv() {
	$day_row_array_rand = getDataAsArray();
	$page_name = '';
	// print_r($day_row_array_rand);
	// $bg_img_maindiv_img = plugins_url() . "/CRJAdventCalendar" . "/images/pexels-photo-76931-900x600.jpeg";
	echo '<div class="container">'."\n";
	// echo '<img src="' . $bg_img_maindiv_img . '" class="bg_img_maindiv_img">';

	// spaltenweise
	echo '<div class="row bg-img-maindiv">'."\n";
	foreach ( $day_row_array_rand as $col ) {
		try {
			echo ' <div class="col-4 col-sm-3 col-md-2">';
			if ($col['day'] != false) {
				echo '<a href="' . $page_name . '?day_nr=' . $col['day'] . '">';
				echo '<img src="' . $col['img'] . '" alt="Tag: ' . $col['day'] . '" title="Tag: ' . $col['day'] . '"/>';
				echo '</a>';
			}
			echo '</div>' . "\n";
		} catch ( Exception $e ) {
			echo "<br>Probleme";
		}
	}
	echo '</div>'."\n";

	echo '</div><!-- END  container -->';
}
function show_FrontendPagetmp() {
	/*

	*/
}
function show_FrontendPage() {
	// @todo optionwerte fuer h1 und h2; solle info leiste oben angezeigt werden?

	// variable
	$howAdventInfo = TRUE;
	$pageTitle = get_option ( 'crj_advent_text_title' );
	$pageSubTitle = get_option ( 'crj_advent_text_subtitle' );

	$bg_img_main_img = plugins_url() . "/CRJAdventCalendar" . "/images/himmel02.jpg";
	$bg_img_maindiv_img = plugins_url() . "/CRJAdventCalendar" . "/images/pexels-photo-76931-900x600.jpeg";

?>
	<style>
		.bg-img-main {
			background-color: #000;
			/*
			background-image: linear-gradient(0deg, #ffffff 0%, #2565A6 44%);
			padding-bottom: 20px;
			background: url(<?php echo $bg_img_main_img; ?>) no-repeat;
			background-size: 100% 100%;
			position: absolute;*/
		}
		.bg-img-maindiv {
			background: url(<?php echo $bg_img_maindiv_img; ?>) no-repeat;
			background-size: 100% 100%;
		}
	</style>

	<div class="bg-img-main" id="wrapperAdvent">

	<?php
		if ($howAdventInfo == TRUE) {
			echo '<div class="text-center text-white" id="Advent">';
			echo getAdventInfo();
			echo '</div><!-- END  header-->';
		}
	?>

		<div class="container">

			<div class="header-advent text-center">
				<br/>
				<h1 class="text-white title"><?php echo $pageTitle; ?></h1>
			</div>

			<?php
			// getHtmlAdventskalenderAsTree();
			getHtmlAdventskalenderAsDiv();
			?>
			<div class="header-advent text-center">
				<br/>
				<h2 class="text-white sub-title"><?php echo $pageSubTitle; ?></h2>
			</div>

			<br/>

			<p class="text-center">
				<a href="http://www.campusradio-jena.de" class="goBack">zurück zur Campusradio Seite</a>
			</p>
		</div><!-- END container-->

		<?php
			if (isset($_GET['day_nr']) AND ( $_GET['day_nr'] >= 1) AND ($_GET['day_nr'] <= 24) ) {

				//$day_nr = get_post($_GET['day_nr']);
				$day_nr = $_GET['day_nr'];
				$pageURL = "https://" . $_SERVER['SERVER_NAME'] . "/adventskalender/";

				$month = 12;
				$day_server = $day_nr;

				//cheater methode

				// CheatMode Datum ueberprüfung: 0 = ausgeschalten; 1 = wird geprueft
				if (get_option ( 'crj_advent_cheatMode' ) == "0") {
					//echo "cheating on";
					//$day = $_GET['day_nr'];
					//$day_server = $day_nr;
				} else {
					//vom server tag und monat geben lassen
					$day_server = date('d');
					$month = date('m');
				}

				if (($day_server >= $day_nr) and ($month == '12')) {

					$post_id = get_option('crj_advent_link_tag' . $day_nr);

					// den ensprechenden post laden
					$queried_post = get_post($post_id);
					$content = $queried_post->post_content;
					$content = apply_filters('the_content', $content);
					$content = str_replace(']]>', ']]&gt;', $content);

					echo '<article id="post-'. $post_id .'" class="boxAdvent">'. "\n";

					echo '<header class="advent-post-header">'. "\n";

					echo '  <h3>Türchen Nummer: '. $day_nr.'</h3>'. "\n";
					echo '</header><!-- .entry-header -->'. "\n";

					// content/textausgabe
					echo '<div class="advent-post-content">' . "\n";
					echo $content . "\n";
					echo '</div>' . "\n";

					echo '<footer class="advent-post-footer">';
					echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
					echo '</footer>'. "\n";

					echo '</article><!-- #post -->'. "\n";


				} else if ($month != '12') {

					echo '<article id="boxAdvent_error" class="boxAdvent_error">'. "\n";

					echo '<header class="advent-post-header">'. "\n";
					echo '  <h3>ja ist denn schon Dezember oder was?</h3>'. "\n";
					echo '</header><!-- .entry-header -->'. "\n";

					// content/textausgabe
					echo '<div class="advent-post-content">' . "\n";
					echo '<img src="'. plugins_url () .'/CRJAdventCalendar/images/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
					echo '<br/>';
					echo 'Mogeln gilt nicht.';
					echo '</div>' . "\n";

					echo '<footer class="advent-post-footer">';
					echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
					echo '</footer>'. "\n";

					echo '</article><!-- #post -->'. "\n";

				} else {
					echo '<article id="boxAdvent_error" class="boxAdvent_error">'. "\n";

					echo '<header class="advent-post-header">'. "\n";
					echo '  <h3>Böse Kinder kriegen nix!</h3>'. "\n";
					echo '</header><!-- .entry-header -->'. "\n";

					// content/textausgabe
					echo '<div class="advent-post-content">' . "\n";
					echo '<img src="'.plugins_url ().'/CRJAdventCalendar/images/w-mann01-ani.gif" width="65" height="103" border="0" alt="bad" align="middle">';
					echo '<br/>';
					echo 'Mogeln gilt nicht, nur die Türchen für den heutigen oder einen früheren Tag öffnen. ';
					echo '</div>' . "\n";

					echo '<footer class="advent-post-footer">';
					echo  '<a href="' . $pageURL . '">Fenster schlie&szlig;en!</a>';
					echo '</footer>'. "\n";

					echo '</article><!-- #post -->'. "\n";
				}
			}
		?>

	</div><!-- END  wrapper-->

	<?php
		getHtmlPiwikCode();
}

function readRestAPIadventeintrag(){
	$api_url = "https://www.campusradio-jena.de/wp-json/wp/v2/adventeintrag?per_page=100";
	$data = json_decode(join("", file($api_url)));
	return $data;
}

function createOptionField($data, $day) {

	$html_output = '<select name="crj_advent_link_tag' . $day . '">';

	foreach ($data as $result) {
		if (get_option ( 'crj_advent_link_tag' . $day ) == $result->id) {
			$html_output .= '<option value="' . $result->id . '" selected>' . $result->title->rendered . '</option>';
		}
		else {
			$html_output .= '<option value="' . $result->id . '">' . $result->title->rendered . '</option>';
		}
	}

	$html_output .= '</select>';

	return $html_output;
}
/**
 * ********************************************************************************************************
 *
 * Backend
 *
 * wird nur angezeiht wenn sich ein gilt nur wenn User in Admin Bereich ist
 *
 * ********************************************************************************************************
 */

/**
		show Setting page
*/
function ShowBackendSettingsPage() {

	$data = readRestAPIadventeintrag();

	// wenn der speicher button gedrueckt wurde, aktualisiere die einzelenen Variablen
	if (isset ( $_POST ['submit_save'] )) {
		updateLinksToAdventPosts();
		echo "aktualisierte Daten wurden abgespeichert:";
	}

	$html_output = '<div id="Plugin_Wrapper">';
	$html_output .= '<div id="Plugin_Wrapper_Content">';

	$html_output .= "\n" . '<!--start Themplate_Option_Page_Show-->' . "\n";

	$html_output .= '<form name="form1" method="post" action="#">';

	$html_output .= '<h2 class="text-bold">Einstellungen fuer den Adventskalender</h2>' . "\n";
	$html_output .= 'Link: <a href="https://www.campusradio-jena.de/adventskalender">https://www.campusradio-jena.de/adventskalender</a>';

	$html_output .= '<br/>' . "\n";
	$html_output .= '<hr/>' . "\n";
	$html_output .= '<br/>' . "\n";
// einstellungen
	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Status der Ueberpruefung des Datums: ';
	$html_output .= '<select name="crj_advent_cheatMode">';
	if (get_option ( 'crj_advent_cheatMode' ) == 0) {
		$html_output .= '<option value="0" selected>aus (Cheatmode)</option>';
		$html_output .= '<option value="1">ein</option>';
	}
	else {
		$html_output .= '<option value="0">aus (Cheatmode)</option>';
		$html_output .= '<option value="1" selected>ein</option>';
	}
	$html_output .= '</select>';

	$html_output .= '</div>';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";

	$html_output .= '<br/>' . "\n";
	$html_output .= '<hr/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Hauptueberschrift h1';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= '<input name="crj_advent_text_title" size="50" maxlength="150" type="text" value="' . get_option ( 'crj_advent_text_title' ) . '" />';
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";
	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Hauptueberschrift h2';
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= '<input name="crj_advent_text_subtitle" size="50" maxlength="150" type="text" value="' . get_option ( 'crj_advent_text_subtitle' ) . '" />';
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";

	$html_output .= '<br/>' . "\n";
	$html_output .= '<hr/>' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Das Datum bezieht sich immer auf das aktuelle ('. date('Y'). ') Jahr.';
	$html_output .= '</div>';
	$html_output .= '<br/>' . "\n";

// zuordnung der einzelnen Tage
	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 1, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '01');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 2, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '02');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 3, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '03');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 4, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '04');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 5, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '05');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 6, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '06');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 7, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '07');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 8, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '08');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 9, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '09');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 10, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '10');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 11, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '11');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 12, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '12');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 13, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '13');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 14, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '14');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 15, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '15');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 16, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '16');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 17, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '17');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 18, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '18');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 19, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '19');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 20, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '20');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 21, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '21');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 22, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data,'22');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 23, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '23');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div class="infotext_2">';
	$html_output .= '<div class="text-left text-bold">';
	$html_output .= 'Tag: ' . date('d (l)', mktime(0, 0, 0, 12, 24, date('Y')));
	$html_output .= '</div>';
	$html_output .= '<div class="text_content">';
	$html_output .= createOptionField($data, '24');
	$html_output .= '</div>';
	$html_output .= '</div>' . '<!-- END infotext -->' . "\n";
	$html_output .= '<br/>' . "\n";

	$html_output .= '<div id="back_link">' . "\n";
	$html_output .= '<input name="submit_save" type="submit" value="Speichern">' . "\n";
	$html_output .= '</div>' . "\n";

	$html_output .= '</form>';
	$html_output .= '</div>' . '<!-- END Plugin_Wrapper_Content -->' . "\n";
	$html_output .= '<!--end Themplate_Option_Page_Show-->' . "\n" . "\n";

	echo $html_output;

}
